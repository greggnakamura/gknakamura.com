/* global Timesheet */

(function () {
    'use strict';

    var timeSheetExists = document.getElementById('timesheet-default');

    if (timeSheetExists) {
        new Timesheet('timesheet-default', 2007, 2018, [
            ['2008', 'PHP'],
            ['2015', 'PHP / WordPress'],
            ['09/2008', '2016', 'ASP.Net (C#), SQL Server', 'dolor'],
            ['09/2008', '2016', 'HTML', 'lorem'],
            ['2013', '2018', 'HTML5', 'lorem'],
            ['09/2008', '2016', 'CSS', 'lorem'],
            ['2013', '2018', 'CSS3', 'lorem'],
            ['2009', '2018', 'Javascript', 'ipsum'],
            ['2012', '2016', 'jQuery', 'ipsum'],
            ['2015', '2018', 'AngularJs', 'ipsum']
        ]);
    }
})();
